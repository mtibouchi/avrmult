#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include "avr_mcu_section.h"
AVR_MCU(F_CPU, "atmega164");

/* USART for ATmega164 */
#define BAUD 38400
#include <util/setbaud.h>

void usart_init( void ) {
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

    UCSR0B = (1 << RXEN0) | (1 << TXEN0);

    UCSR0C = (1 << USBS0) | (3 << UCSZ00);
}

void usart_putc( unsigned char data ) {
    while( !(UCSR0A & (1<<UDRE0)) )
	;
    UDR0 = data;
}

unsigned char usart_getc( void ) {
    while( !(UCSR0A & (1<<RXC0)) )
	;
    return UDR0;
}

void usart_puts(char *s) {
    while(*s)
	usart_putc(*s++);
}

#include <stdint.h>

#define SIZEX 		16
#define STRINGIFY_(x)	#x
#define STRINGIFY(x)	STRINGIFY_(x)
#define SIZEX_STR	STRINGIFY(SIZEX)

uint8_t x[SIZEX];
uint8_t y[SIZEX];
uint8_t z[SIZEX+SIZEX];

void mult(void) {
    int i,j;
    unsigned int m = 0;

    for(i = 0; i < sizeof z; i++)
	z[i] = 0;

    for(i = 0; i < sizeof x; i++) {
	for(j = 0; j < sizeof y; j++) {
	    m+= x[i] * y[j]; // promotion to 16 bits?
	    m+= z[i+j];
	    z[i+j] = (uint8_t) m;
	    m >>= 8; // store carry.
	}
	while( m!= 0 ) { // propagate carry
	    m+= z[i+j];
	    z[i+j] = (uint8_t) m;
	    m >>= 8;
	    j++;
	}
    }
}

#define nibble(x)	(((x)<10)?('0'+(x)):('a'+(x)-10))

void printhex( uint8_t * n, unsigned int len ) {
    int i;
    uint8_t byte;

    usart_putc('0');
    usart_putc('x');

    for(i = len-1; i>=0; i--) {
	byte = n[i];
	usart_putc(nibble(byte >> 4));
	usart_putc(nibble(byte & 0xf));
    }
}

void gethex( uint8_t * n, unsigned int len ) {
    int i;
    uint8_t byte;
    char c;

    for(i = len-1; i>=0; i--) {
	c = usart_getc();
	byte = (c <= '9')?(c-'0'):(c-'a'+10);
	byte <<= 4;
	c = usart_getc();
	byte|= (c <= '9')?(c-'0'):(c-'a'+10);
	n[i] = byte;
    }

    do {
	c = usart_getc();
    } while( c!='\n' && c!='\r' );
}

int main()
{
    usart_init();

    _delay_ms(200);

    usart_puts( "\nInput two " SIZEX_STR "-byte hex numbers one by line:\n\n" );
    gethex( x, sizeof x );
    gethex( y, sizeof y );

    usart_puts( "\nYou entered:\nx = " );
    printhex( x, sizeof x );
    usart_puts( "\ny = " );
    printhex( y, sizeof y );
    usart_puts( "\n\nTheir product is:\nx * y = " );

    mult();

    printhex( z, sizeof z );
    usart_puts( "\n\n" );

    _delay_ms(200);
    sleep_cpu();
}
