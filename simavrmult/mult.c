#include <stdlib.h>
#include <stdio.h>
#include <libgen.h>
#include <signal.h>

#include "sim_avr.h"
#include "avr_ioport.h"
#include "sim_elf.h"

#include "uart_pty.h"

avr_t * avr = NULL;
uart_pty_t uart_pty;

void sig_int(int sign)
{
    printf("signal caught, simavr terminating\n");
    if (avr)
	    avr_terminate(avr);
    exit(0);
}

int main(int argc, char *argv[])
{
    elf_firmware_t f;
    const char * fname = "atmega164p_mult.axf";
    elf_read_firmware(fname, &f);

    printf("firmware %s f=%d mmcu=%s\n", fname, (int) f.frequency, f.mmcu);

    avr = avr_make_mcu_by_name(f.mmcu);
    if (!avr) {
        fprintf(stderr, "%s: AVR '%s' not known\n", argv[0], f.mmcu);
        exit(1);
    }

    avr_init(avr);
    avr_load_firmware(avr, &f);

    uart_pty_init(avr, &uart_pty);
    uart_pty_connect(&uart_pty, '0');

    signal(SIGINT, sig_int);
    signal(SIGTERM, sig_int);

    for (;;) {
	int state = avr_run(avr);
	if ( state == cpu_Done || state == cpu_Crashed)
	    break;
    }

    uart_pty_stop(&uart_pty);
    avr_terminate(avr);

    return 0;
}
