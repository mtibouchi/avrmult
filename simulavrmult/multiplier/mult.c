/*
 *  This is a test program to demonstrate use of the debug input and output
 *  port.
 *
 *  $Id$
 */

#include <stdint.h>

#define SIZEX 		16
#define STRINGIFY_(x)	#x
#define STRINGIFY(x)	STRINGIFY_(x)
#define SIZEX_STR	STRINGIFY(SIZEX)

/*
 *  This port correponds to the "-W 0x20,-" command line option.
 */
#define special_output_port (*( (volatile char *)0x20))

/*
 *  This port correponds to the "-R 0x22,-" command line option.
 */
#define special_input_port  (*( (volatile char *)0x22))

uint8_t x[SIZEX];
uint8_t y[SIZEX];
uint8_t z[SIZEX+SIZEX];

/*
 *  Poll the specified string out the debug port.
 */
void debug_puts(const char *str)
{
  const char *c;

  for ( c=str ; *c ; c++ )
    special_output_port = *c;
}

void mult(void) {
    int i,j;
    unsigned int m = 0;

    for(i = 0; i < sizeof z; i++)
	z[i] = 0;

    for(i = 0; i < sizeof x; i++) {
	for(j = 0; j < sizeof y; j++) {
	    m+= x[i] * y[j]; // promotion to 16 bits?
	    m+= z[i+j];
	    z[i+j] = (uint8_t) m;
	    m >>= 8; // store carry.
	}
	while( m!= 0 ) { // propagate carry
	    m+= z[i+j];
	    z[i+j] = (uint8_t) m;
	    m >>= 8;
	    j++;
	}
    }
}

#define nibble(x)	(((x)<10)?('0'+(x)):('a'+(x)-10))

void debug_printhex( uint8_t * n, unsigned int len ) {
    int i;
    uint8_t byte;

    debug_puts( "0x" );

    for(i = len-1; i>=0; i--) {
	byte = n[i];
	special_output_port = nibble(byte >> 4);
	special_output_port = nibble(byte & 0xf);
    }
}

void debug_gethex( uint8_t * n, unsigned int len ) {
    int i;
    uint8_t byte;
    char c;

    for(i = len-1; i>=0; i--) {
	c = special_input_port;
	byte = (c <= '9')?(c-'0'):(c-'a'+10);
	byte <<= 4;
	c = special_input_port;
	byte|= (c <= '9')?(c-'0'):(c-'a'+10);
	n[i] = byte;
    }

    do {
	c = special_input_port;
    } while( c!='\n' );
}

/*
 *  Main for test program.  Enter a string and echo it.
 */
int main()
{
  debug_puts( "\nInput two " SIZEX_STR "-byte hex numbers one by line:\n\n" );
  debug_gethex( x, sizeof x );
  debug_gethex( y, sizeof y );

  debug_puts( "\nYou entered:\nx = " );
  debug_printhex( x, sizeof x );
  debug_puts( "\ny = " );
  debug_printhex( y, sizeof y );
  debug_puts( "\n\nTheir product is:\nx * y = " );

  mult();

  debug_printhex( z, sizeof z );
  debug_puts( "\n\n" );

  return 0;
}
