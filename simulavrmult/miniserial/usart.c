#include "usart.h"
#include <avr/io.h>

#if defined (__AVR_ATmega8515__)

void usart_init( unsigned int baud ) {
    UBRRH = (unsigned char)(baud >> 8);
    UBRRL = (unsigned char)baud;

    UCSRB = (1 << RXEN) | (1 << TXEN);

    UCSRC = (1 << URSEL) | (1 << USBS) | (3 << UCSZ0);
}

void usart_putc( unsigned char data ) {
    while( !(UCSRA & (1<<UDRE)) )
	;
    UDR = data;
}

unsigned char usart_getc( void ) {
    while( !(UCSRA & (1<<RXC)) )
	;
    return UDR;
}

void usart_flush( void ) {
    unsigned char dummy;

    while( UCSRA & (1<<RXC) )
	dummy = UDR;
}

#else /* AT90s8515 */

void usart_init( unsigned int baud ) {
    UBRR = (unsigned char)baud;

    UCR = (1 << RXEN) | (1 << TXEN);
}

void usart_putc( unsigned char data ) {
    while( !(USR & (1<<UDRE)) )
	;
    UDR = data;
}

unsigned char usart_getc( void ) {
    while( !(USR & (1<<RXC)) )
	;
    return UDR;
}

void usart_flush( void ) {
    unsigned char dummy;

    while( USR & (1<<RXC) )
	dummy = UDR;
}

#endif
