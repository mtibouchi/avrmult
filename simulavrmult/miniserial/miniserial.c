/*
 *  This is a test program to demonstrate use of the debug input and output
 *  port.
 *
 *  $Id$
 */

#include <stdint.h>
#include "usart.h"

#define BAUD 9600
#define BVAL (((F_CPU / 16) / BAUD) - 1)

void usart_puts(char *s) {
    while(*s)
	usart_putc(*s++);
}

int main()
{
    usart_init(BVAL);

    usart_puts("Hello World!\n");

    return 0;
}
