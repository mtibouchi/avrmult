void usart_init(unsigned int);
void usart_putc(unsigned char);
unsigned char usart_getc(void);
void usart_flush(void);
