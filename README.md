# AVRMult

Trying to implement and simulate large integer multiplication on
AVR microcontrollers and AVR-based smartcards (ATmega8515 and/or
ATmega163 funcards), and obtain traces using SASEBO-W & ChipWhisperer.

Subprojects:

- **simulavrmult**: large integer multiplication on AT90S8515 simulated
  with `simulavr`.

- **simavrmult**: large integer multiplication on ATmega164p simulated
  with `simavr`, with proper USART communication in a pty.

- **sossemult**: modification of the SOSSE smartcard OS to work on
  ATmega163 and implement an additional instruction for large integer
  multiplication.

- **cpmult**: ChipWhisperer-based attack on `sossemult` with SASEBO-W
  and OpenADC.

